

import csv
import numpy as np
import matplotlib.pyplot as plt
import pyransac3d as pyrsc
import scipy

from sklearn.cluster import KMeans, DBSCAN
from skspatial.objects import Plane, Points
from skspatial.plotting import plot_3d
from numpy.random import default_rng
rng = default_rng()

def point_reader():
    with open('point_cloud.xyz', newline='\n') as point_file:
        reader = csv.reader(point_file, delimiter=',')
        for x,y,z in reader:
            yield (float(x),float(y),float(z))



def RANSAC(points,threshold,MAX_ITER):
    best_model_size=0
    best_inliers = np.array(0)
    iterator = 0
    while(iterator < MAX_ITER):
        maybe_inliers = rng.permutation(points)[:3]
        A,B,C = zip(*maybe_inliers)
        A = np.array(A)
        B = np.array(B)
        C = np.array(C)

        vec_a = A - C
        vec_b = B - C

        vec_u_a = vec_a/np.linalg.norm(vec_a)
        vec_u_b = vec_b/np.linalg.norm(vec_b)
        vec_w = np.cross(vec_u_a, vec_u_b)
        d = -np.sum(np.multiply(vec_w,C))

        distances_all_points = (vec_w*points + d)/np.linalg.norm(vec_w)
        inliers_indices = np.where(np.abs(distances_all_points) <= threshold)[0]

        inliers_list = []
        for i in inliers_indices:
            inliers_list.append(points[i])

        model_size = len(inliers_list)


        if model_size > best_model_size:
            best_model_size = model_size
            best_inliners = np.array(inliers_list)
        iterator+=1
    return best_inliners


def is_plane(threshold,normal,d,points):
    distance = 0
    for point in points:
        distance += np.abs(normal[0]*point[0]+normal[1]*point[1]+normal[2]*point[2]+d)/np.sqrt(normal[0]**2 + normal[1]**2 + normal[2]**2)
    if (distance/len(points)) < threshold:
        return 1
    else:
        return 0

def is_vertical(threshold,normal):
    if np.abs(normal[2] )< threshold:
        return 1
    else:
        return 0

def is_horizontal(threshold,normal):
    if (np.abs(normal[1]) < threshold) and (np.abs(normal[0]) < threshold):
        return 1
    else:
        return 0


if __name__ == '__main__':
    threshold = 0.5
    MAX_ITER = 100

    clusters = list(point_reader())
    x,y,z = zip(*clusters)

    print("***  Clustering and ransac using KMeans and self-implemented RANSAC ***")

    clusterer = KMeans(n_clusters=3)
    X = np.array(clusters)
    y_pred = clusterer.fit_predict(X)

    red = y_pred == 0
    blue = y_pred == 1
    green = y_pred == 2




    RED_inliers = RANSAC(X[red],0.5,100)
    RED_plane = Plane.best_fit(RED_inliers)

    BLUE_inliers = RANSAC(X[blue],0.5,100)
    BLUE_plane = Plane.best_fit(BLUE_inliers)\

    GREEN_inliers = RANSAC(X[green],0.5,100)
    GREEN_plane = Plane.best_fit(GREEN_inliers)


    RED_normal=np.array(RED_plane.normal)
    RED_d = -RED_plane.point.dot(RED_normal)

    BLUE_normal=np.array(BLUE_plane.normal)
    BLUE_d = -BLUE_plane.point.dot(BLUE_normal)

    GREEN_normal=np.array(GREEN_plane.normal)
    GREEN_d = -GREEN_plane.point.dot(GREEN_normal)
    print("Red cluster normal vector:", RED_plane)
    print("Blue cluster normal vector:", BLUE_plane)
    print("Green cluster normal vector:", GREEN_plane)

    print("Is red plane a good fit: ", is_plane(20, RED_normal, RED_d, X[red]))
    print("Is blue plane a good fit: ", is_plane(20, BLUE_normal, BLUE_d, X[blue]))
    print("Is green plane a good fit: ", is_plane(20, GREEN_normal, GREEN_d, X[green]))

    print("Is red plane vertical: ", is_vertical(0.01, RED_normal), "or horizontal: ",
          is_horizontal(0.01, RED_normal))
    print("Is blue plane vertical: ", is_vertical(0.01, BLUE_normal), "or horizontal: ",
          is_horizontal(0.01, BLUE_normal))
    print("Is green plane vertical: ", is_vertical(0.01, GREEN_normal), "or horizontal: ",
          is_horizontal(0.01, GREEN_normal))


    fig1= plt.figure()
    ax = fig1.add_subplot(projection='3d')
    ax.scatter(X[red,0], X[red,1], X[red,2], c="r", marker=",", s=0.5)
    ax.scatter(X[blue,0], X[blue,1], X[blue,2], c="b", marker=",", s=0.5)
    ax.scatter(X[green,0], X[green,1], X[green,2], c="g", marker=",", s=0.5)

    xx, yy = np.meshgrid(range(100), range(100))
    r_z = (-RED_normal[0] * xx - RED_normal[1] * yy -  RED_d) * 1. /RED_normal[2]
    b_z = (-BLUE_normal[0] * xx - BLUE_normal[1] * yy - BLUE_d) * 1. / BLUE_normal[2]
    g_z = (-GREEN_normal[0] *xx - GREEN_normal[1] * yy - GREEN_d) * 1. / GREEN_normal[2]

    ax.plot_surface(xx, yy, r_z, alpha=0.2, color="red")
    ax.plot_surface(xx, yy, b_z, alpha=0.2, color="blue")
    ax.plot_surface(xx, yy, g_z, alpha=0.2, color="green")

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    fig1.tight_layout()
    ax.set_title("Three clusters found using KMeans method")


    """ Clustering and ransac using DBSCAN and pyRANSAC """
    print("***  Clustering and ransac using DBSCAN and pyRANSAC ***")
    db_clusterer= DBSCAN(eps=15, min_samples=5)
    db_y_pred = db_clusterer.fit_predict(X)

    cyan = y_pred == 0
    magenta = y_pred == 1
    yellow = y_pred == 2

    cyan_plane = pyrsc.Plane()
    cyan_best_eq, cyan_best_inliers = cyan_plane.fit(X[cyan], 0.5)

    magenta_plane = pyrsc.Plane()
    magenta_best_eq, magenta_best_inliers = magenta_plane.fit(X[magenta], 0.5)

    yellow_plane = pyrsc.Plane()
    yellow_best_eq, yellow_best_inliers = yellow_plane.fit(X[cyan], 0.5)

    print("Cyan cluster normal vector:", cyan_best_eq)
    print("Magenta cluster normal vector:", magenta_best_eq)
    print("Yellow cluster normal vector:", yellow_best_eq)

    print("Is cyan plane a good fit: ", is_plane(20, cyan_best_eq[:3], cyan_best_eq[2], X[cyan]))
    print("Is magenta plane a good fit: ", is_plane(20, magenta_best_eq[:3], magenta_best_eq[2], X[magenta]))
    print("Is yellow plane a good fit: ", is_plane(20, yellow_best_eq[:3], yellow_best_eq[2], X[yellow]))

    print("Is cyan plane vertical: ", is_vertical(0.01, cyan_best_eq[:3]), "or horizontal: ",
          is_horizontal(0.01, cyan_best_eq[:3]))
    print("Is magenta plane vertical: ", is_vertical(0.01, magenta_best_eq[:3]), "or horizontal: ",
          is_horizontal(0.01, magenta_best_eq[:3]))
    print("Is yellow plane vertical: ", is_vertical(0.01, yellow_best_eq[:3]), "or horizontal: ",
          is_horizontal(0.01, yellow_best_eq[:3]))

    fig2= plt.figure()
    bx = fig2.add_subplot(projection='3d')
    bx.scatter(X[cyan,0], X[cyan,1], X[cyan,2], c="c", marker=",", s=0.5)
    bx.scatter(X[magenta,0], X[magenta,1], X[magenta,2], c="m", marker=",", s=0.5)
    bx.scatter(X[yellow,0], X[yellow,1], X[yellow,2], c="y", marker=",", s=0.5)

    c_z = (-cyan_best_eq[0] * xx - cyan_best_eq[1] * yy - cyan_best_eq[3]) * 1. / cyan_best_eq[2]
    m_z = (-magenta_best_eq[0] * xx - magenta_best_eq[1] * yy - magenta_best_eq[3]) * 1. / magenta_best_eq[2]
    y_z = (-yellow_best_eq[0] * xx - yellow_best_eq[1] * yy - yellow_best_eq[3]) * 1. / yellow_best_eq[2]

    bx.plot_surface(xx, yy, c_z, alpha=0.2, color="cyan")
    bx.plot_surface(xx, yy, m_z, alpha=0.2, color="magenta")
    bx.plot_surface(xx, yy, y_z, alpha=0.2, color="yellow")

    bx.set_xlabel('X')
    bx.set_ylabel('Y')
    bx.set_zlabel('Z')
    fig2.tight_layout()
    bx.set_title("Three clusters found using DBSCAN method")
    plt.show()



